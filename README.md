# msp430-proximity
**MSP430 Proximity Sensor**

An optical proximity sensor compiled of a Texas Instruments MSP430 and different LEDs and Photodiodes.


**More Information**

On the MSP 430 from *Texas Instruments*
https://www.ti.com/product/MSP430G2452

On the GCC Toolchain under Linux (in german) from *Ubuntuusers.de*
https://wiki.ubuntuusers.de/Archiv/MSP430-Toolchain/


**Get started on Linux**

 To run on Linux Following https://wiki.ubuntuusers.de/Archiv/MSP430-Toolchain/
 Needed Packages (under Manjaro available as AUR)
  binutils-msp430
  gcc-msp430
  gdb-msp430
  msp430-libc
  msp430mcu
  mspdebug

 compile with (adjust to your MSP430 device!) 
 msp430-gcc -mmcu=msp430g2452 -o  proximity.out ./proximity.c

  Then run with 
   $ sudo mspdebug rf2500
   (mspdebug) prog proximity
   (mspdebug) run

**Layout:**

<pre>
                    ________   
          VCC  ----|o       |---- VSS  GND   
   LED1   P1.0 ----|        |---- P2.6 XIN
   UART   P1.1 ----|        |---- P2.7 XOUT
   UART   P1.2 ----|        |---- TEST
   S2     P1.3 ----|        |---- nRST S1
   ADC In P1.4 ----|        |---- P1.7 External LED / Illumination
          P1.5 ----|        |---- P1.6 LED2
    LED   P2.0 ----|        |---- P2.5  LED 
    LED   P2.1 ----|        |---- P2.4  LED 
    LED   P2.2 ----|        |---- P2.3  LED
                    --------
  
</pre>

**Wiring:**

  For this project two orange LEDs were used. One for Illumination and one for detection.
  Illumination Was done with 33 Ohm between Pin 1.7 and LED.  
  Detection was realized with an LED directly to the ADC (Anode) and GND (Cathode). 
  For reference, the Anode is also connected via 2M Ohm to VCC for reference. This leaves us 
  with a very bad signal and an upgrade would be a nice transimpedance amplifier (and of course a "real" Photodiode). 
  Nevertheless, by Setting the ADC to the lowest available reference voltage, and using 
  the longest sample times, we can create basic functionality.
  
<pre>

  P1.4---------|>|------GND
           |
           |------|2M|--VCC

  P1.7--|33R|--|>|------GND

</pre>

  Port 2 is used for an LED meter with two red, two yellow and two green LEDs. It is set up with 2.3k Ohm 
  resistors for each LED:

<pre>

  P2.x--|2K3|--|>|------GND

</pre>

**Remarks Software:**

  The software was intentionally build without the use of interrupts. Although
  this would be more elegant, in some fields interrupts are not welcome, as they
  add an additional fault source. 


