// proximity.c is a program to incorporate an optical proximity meter
// on an MSP430G2452.
// (cc) 2021, Johannes Neidhart
//
// compile with (adjust to your MSP430 device!) 
// msp430-gcc -mmcu=msp430g2452 -o  proximity.out ./proximity.c
//
//  Then run with 
//   $ sudo mspdebug rf2500
//   (mspdebug) prog proximity
//   (mspdebug) run
//
//  Layout:
//
//                    ________   
//          VCC  ----|o       |---- VSS  GND   
//   LED1   P1.0 ----|        |---- P2.6 XIN
//   UART   P1.1 ----|        |---- P2.7 XOUT
//   UART   P1.2 ----|        |---- TEST
//   S2     P1.3 ----|        |---- nRST S1
//   ADC In P1.4 ----|        |---- P1.7 External LED / Illumination
//          P1.5 ----|        |---- P1.6 LED2
//    LED   P2.0 ----|        |---- P2.5  LED 
//    LED   P2.1 ----|        |---- P2.4  LED 
//    LED   P2.2 ----|        |---- P2.3  LED
//                    --------


#include  <msp430.h>

unsigned int limit = 855;       // Limit value for detection. Depends on used setup and electronic details.
unsigned int maxValue;          // Value if Object is very close -> Is set during initialization
unsigned int minValue;          // Base value / ambient value if object is not in FoV -> Is set during Initialization

//    Values which work pretty good
//    minValue = 800;
//    maxValue = 860;

void setupHardware(){
    // General Setup
    WDTCTL = WDTPW + WDTHOLD;   // Stop watchdog timer

    // Setup the ADC
    // ADC10 can use multiple clock sources - default is internal ADC10OSC
    ADC10CTL1 |= CONSEQ_0;              // Use Single-Channel-Single-Conversion Mode
    ADC10CTL1 |= INCH_4 + ADC10DIV_7;   // Use ADC channel 4 (A4 at P1.4) and divide clock by 8
    ADC10CTL0 |= REFON;                 // Set internal reference. Runs on 1.5V by default.
    ADC10CTL0 |= SREF_1 + ADC10SHT_3;   // use internal reference, Sample-and-hold time 4 clks
    ADC10CTL0 |= ADC10ON;               // Switch ADC ON after it was configured

    // Setup of Pins of Port 1
    P1DIR  = 0x80; 				// P1.7 output (Illumination)
    P1DIR |= 0x40; 				// P1.6 output (green onboard LED)
    P1OUT  = 0; 				// All outputs set off

    // Setup of Pins of Port 2 / Meter LEDs
    P2DIR = 0x3F;               // All Pins of Port 2 are used as output
    P2OUT = 0;                  // Initially all pins are LOW

}

// Reads the ADC value and returns it as an unsigned integer
unsigned int getADCValue() {
    ADC10CTL0 |= ADC10SC + ENC; // Set Start Sample
    ADC10CTL0 &= ~ADC10SC;      // Unset Start Sample and start conversion
    while ((ADC10CTL1 & ADC10BUSY) == 1);// While ADC busy, wait
    return((int)ADC10MEM);	    // Return value ADC In Pin
}

// very simple wait function 
void wait(long int time) {

    long int t = 10*time;

    while(t){
        t--;
    }

}

void illuminationOn(){
    P1OUT |= 0x80; 			    // P1.7 output (Ilumination) set to HIGH
}

void illuminationOff(){
    P1OUT &= ~0x80; 			// P1.7 output (Ilumination) set to LOW
}

void statusLEDOn(){
    P1OUT |= 0x40;              // Green Onboard LED On
}

void statusLEDOff(){
    P1OUT &=~ 0x40;             // Green Onboard LED OFF
}

// this function will signal an error by flashing all the 
// LEDs of the meter. Reset is necessary.
void errorMode(){
    P2OUT = 0;
    while(1){
        P2OUT ^= 0x3F;
        wait(1000);
    }

}


// Takes the value of the ADC input and sets the meter according to the 
// minValue and maxValue parameters. gaugeValue() needs to be called before.
//
// This function will place pdValue between minValue and maxValue
// and switch the LEDs of the meter based on the position. If pdValue
// is close to maxValue an object is very close and all LEDs are set ON.
// If pdValue is close to minValue the object is further away and only one
// LED is set ON.
void setOutputMeter(unsigned int pdValue){
    unsigned int interval;
    if(maxValue < minValue){
        errorMode();
    }else{
        if(pdValue > minValue){
            interval = maxValue - minValue;
            P2OUT=0x01;             // 1 LED on
            do{     // do-while-false -> not undisputed but very handy sometimes to keep the
                // structure flat. Used to break during a sequence of if statements.
                if(pdValue-minValue > 5*interval/6){
                    P2OUT =0x3F;    // all LEDs on
                    break;
                }
                if(pdValue-minValue > 4*interval/6){
                    P2OUT =0x1F;    // 5 LEDs on
                    break;
                }
                if(pdValue-minValue > 3*interval/6){
                    P2OUT =0x0F;    // 4 LEDs on
                    break;
                }
                if(pdValue-minValue > 2*interval/6){
                    P2OUT =0x07;    // 3 LEDs on
                    break;
                }

                if(pdValue-minValue > 1*interval/6){
                    P2OUT =0x03;    // 2 LEDs on
                    break;
                }

            }while(0);
        }else{
            P2OUT = 0;
        }
    }
}


// Get the ADC value and compare to a limit. Set the on-board
// LED if object is close.
void measureAndEvaluate(){
    unsigned int pdVal;

    wait(10);   // start with a short break so that illumination can settle

    pdVal = getADCValue();
    if(pdVal > limit){
        statusLEDOn();
    }else{
        statusLEDOff();
    }
    setOutputMeter(pdVal);
}


// In order to work for different ambient conditions the range of 
// ADC values for the measurement needs to be set. To do this the 
// function will record two values, one without an object in the 
// Field-of-View and one with an object very close. The Status Meter
// LEDs are flashed inbetween to signal shortly, before the measurement 
// will take place.
void gaugeLevels(){
    unsigned int i;

    P2OUT = 0;
    for(i = 0; i<5; i++){
        wait(1000);
        P2OUT ^= 0x3F;
    }
    illuminationOff();
    wait(10);
    minValue = getADCValue()+45;
    wait(1000);
    P2OUT = 0;
    wait(10000);
    illuminationOn();
    wait(10000);
    maxValue = getADCValue();
    illuminationOff();
}


int main() {
    // Initialize
    setupHardware();

    // Start gauge routine to set range of measurement
    gaugeLevels();

    // switch on illumination
    illuminationOn();


    // Start Loop
    while(1){
        measureAndEvaluate();
        wait(100);
    }
    return(0);
}
